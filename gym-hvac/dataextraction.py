import pandas as pd
import csv
import matplotlib.pyplot as plt


filename = 'D:/Study Material/Clean Energy/CleanEnergyHVACGroup-master/Dataset2.csv'
cols = ['DATE','HourlyDewPointTemperature']
data = pd.read_csv(filename,parse_dates=['DATE'])

data.set_index("DATE",inplace=True)
data['Temperature']=pd.to_numeric(data['Temperature'],errors='coerce')
data2=pd.DataFrame()

data2['Temperature']=data.Temperature.resample('15min').mean()

data2=data2.interpolate(method='linear')
print(data2.index)

data2.to_csv('D:/Study Material/Clean Energy/CleanEnergyHVACGroup-master/Dataset.csv')




